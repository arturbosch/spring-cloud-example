package com.gitlab.artismarti

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@SpringBootApplication
@EnableEurekaServer
open class EurekaServiceApplication

fun main(args: Array<String>) {
	SpringApplication.run(EurekaServiceApplication::class.java, *args)
}
