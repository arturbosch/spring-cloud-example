package com.gitlab.artismarti

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.client.loadbalancer.LoadBalanced
import org.springframework.cloud.netflix.zuul.EnableZuulProxy
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.messaging.Source
import org.springframework.context.annotation.Bean
import org.springframework.core.ParameterizedTypeReference
import org.springframework.hateoas.Resources
import org.springframework.http.HttpMethod
import org.springframework.messaging.support.MessageBuilder
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate

@EnableZuulProxy
@EnableBinding(Source::class)
@EnableDiscoveryClient
@EnableCircuitBreaker
@SpringBootApplication
open class ReservationClientApplication {
	@Bean
	@LoadBalanced
	open fun restTemplate(): RestTemplate {
		return RestTemplate()
	}
}

fun main(args: Array<String>) {
	SpringApplication.run(ReservationClientApplication::class.java, *args)
}

@RestController
@RequestMapping("/reservations")
open class ReservationApiGatewayController @Autowired constructor(open val restTemplate: RestTemplate) {

	open fun fallback(): Collection<String> {
		return arrayListOf("Maaeeeh")
	}

	@HystrixCommand(fallbackMethod = "fallback")
	@RequestMapping("/names", method = arrayOf(RequestMethod.GET))
	open fun names(): Collection<String> {
		return restTemplate.exchange("http://reservation-service/reservations",
				HttpMethod.GET, null,
				object : ParameterizedTypeReference<Resources<Reservation>>() {})
				.body.content.map { it.name }
	}

	@Autowired
	private lateinit var source: Source

	@RequestMapping(method = arrayOf(RequestMethod.POST))
	open fun write(@RequestBody reservation: Reservation) {
		val channel = source.output()
		channel.send(MessageBuilder.withPayload(reservation.name).build())

	}
}

data class Reservation(val name: String)
