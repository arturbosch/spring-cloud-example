package com.gitlab.artismarti;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author artur
 */
@RestController
@RefreshScope
public class Test {

	@Value("${message}")
	private String message;

	@RequestMapping("read")
	public String message() {
		return message;
	}
}
