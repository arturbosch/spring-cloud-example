package com.gitlab.artismarti

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.messaging.Sink
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.integration.annotation.MessageEndpoint
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.stream.Stream
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@SpringBootApplication
@EnableDiscoveryClient
@EnableBinding(Sink::class)
open class ReservationServiceApplication

fun main(args: Array<String>) {
	SpringApplication.run(ReservationServiceApplication::class.java, *args)
}

@RestController
@RefreshScope
open class MessageRestController(@Value("\${message}") open val message: String) {

	@RequestMapping("/message")
	open fun read(): String {
		return message
	}
}

@Component
open class CLR @Autowired constructor(val reservationRepository: ReservationRepository) :
		CommandLineRunner {

	override fun run(vararg args: String?) {
		Stream.of("Artur", "Chris", "Steffen", "Galina", "Xenia", "Anatoliy")
				.forEach { reservationRepository.save(Reservation(name = it)) }

		reservationRepository.findAll().forEach { println(it) }
	}
}

@RepositoryRestResource
interface ReservationRepository : JpaRepository<Reservation, Long>

@Entity
data class Reservation(var name: String = "", @Id @GeneratedValue var id: Long = -1)

@MessageEndpoint
open class ReservationProcessor @Autowired constructor(val reservationRepository: ReservationRepository) {

	@ServiceActivator(inputChannel = "input")
	open fun accept(input: String) {
		reservationRepository.save(Reservation(input))
	}
}
